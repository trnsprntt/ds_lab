FROM alpine:3.5

RUN apk add --update py2-pip
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt

COPY app.py /usr/src/app/
COPY templates/index.html /usr/src/app/templates/
COPY static/egg.jpg /usr/src/app/static/
COPY static/favicon.ico /usr/src/app/static/


#EXPOSE 80
CMD ["python", "/usr/src/app/app.py"]
